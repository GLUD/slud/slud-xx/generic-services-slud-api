package com.glud.slud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenericServicesSludApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenericServicesSludApiApplication.class, args);
	}

}
